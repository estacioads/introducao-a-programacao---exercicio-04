/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio04;

/**
 *
 * @author alunoti
 */

/*



*/

public class Exercicio04 {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        
        int paes, broas;
        double preco_do_pao, preco_da_broa, valor_arrecadado, valor_deposito;
        
        paes = 120;
        broas = 65;
        
        preco_do_pao = 0.12;
        preco_da_broa = 1.50;
        
        valor_arrecadado = (paes * preco_do_pao) + (broas * preco_da_broa);
        valor_deposito = valor_arrecadado * 0.10;
        
        
        System.out.println("O valor total arrecadado foi de " + valor_arrecadado);
        System.out.println("Ao final do dia o valor a ser depositado é de " + valor_deposito);
        
    }
    
}
